package com.todo.config;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;

@Configuration
@Component
public class DatasourceConfig {

    @Autowired
    @Bean
    public DataSource dataSource(DatabaseProperties properties){

        HikariConfig hikariConfig = new HikariConfig();
        hikariConfig.setUsername(properties.getMysql().getUsername());
        hikariConfig.setPassword(properties.getMysql().getPassword());
        hikariConfig.setJdbcUrl(properties.getMysql().getUrl());
        hikariConfig.setDriverClassName(properties.getMysql().getDriver());
        return new HikariDataSource(hikariConfig);
    }

    @Autowired
    @Bean
    public NamedParameterJdbcTemplate namedParameterJdbcTemplate(DataSource dataSource){
        return new NamedParameterJdbcTemplate(dataSource);
    }

}
