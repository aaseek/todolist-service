package com.todo.controller;


import com.todo.entity.GlobalResponse;
import com.todo.entity.Todo;
import com.todo.entity.TodoResponse;
import com.todo.service.TodoService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
public class TodoController {

    private TodoService todoService;

    private static final Logger LOGGER = LoggerFactory.getLogger(TodoController.class);

    @Autowired
    public TodoController(TodoService todoService) {
        this.todoService = todoService;
    }


    @PostMapping(value = "/todotask")
    public ResponseEntity<GlobalResponse> createTodo(@RequestBody TodoResponse todoResponse) {

        String task = todoResponse.getTask();
        boolean check = todoService.checkDuplicateTask(task);
        if (!check) {
            TodoResponse savedTodo = todoService.addTask(todoResponse);
            LOGGER.info("Create todo list , {}", savedTodo);

            if (savedTodo != null) {
                return new ResponseEntity<>(
                        new GlobalResponse(200, "new task successfully inserted", savedTodo), HttpStatus.OK);
            }
            return new ResponseEntity<>(
                    new GlobalResponse(400, "invalid request or duplicate task found", null), HttpStatus.OK);

        }
        return new ResponseEntity<>(
                new GlobalResponse(400, " duplicate task found", null), HttpStatus.OK);

    }


    // @GetMapping/@RequestMapping annotated method handle the HTTP GET request matched with given URI expression.
    @GetMapping(value = "/list")
    public ResponseEntity<GlobalResponse> getAllTask() {

        List<Todo> taskList = todoService.getAllTask();
        LOGGER.info("List all the task,{}", taskList);

        if (taskList != null) {
            return new ResponseEntity<>(
                    new GlobalResponse(200, "all task listed succesfully", taskList), HttpStatus.OK);
        }
        return new ResponseEntity<>(new GlobalResponse(400, "invalid request", null), HttpStatus.OK);

    }

    @GetMapping(value = "/delete/{id}")
    public ResponseEntity<GlobalResponse> deleteById(@PathVariable("id") int id) {
        int result = todoService.deleteTask(id);
        LOGGER.info("The deleted task is, {} ", result);
        return new ResponseEntity<>(
                new GlobalResponse(200, "task successfully deleted", result), HttpStatus.OK);

    }


    // @PostMapping/RequestMapping(method= RequestMethod.POST) annotated methods handle the HTTP post requests matched with given URI expression.
    @PostMapping(value = "/updatetask/{id}")
    public ResponseEntity<GlobalResponse> updateTask(@RequestBody Todo todo,
                                                     @PathVariable("id") int id) {

        int updateId = todoService.updateTask(todo, id);
        LOGGER.info("The value of updated task is, {}", updateId);
        if (updateId != 0) {
            return new ResponseEntity<>(
                    new GlobalResponse(200, "task has been successfully updated", updateId), HttpStatus.OK);
        }
        return new ResponseEntity<>(
                new GlobalResponse(400, "invalid request", null), HttpStatus.OK);

    }

}

