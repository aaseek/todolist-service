package com.todo.entity;

import java.util.Date;

public class Todo {

    private int id;
    private String task;
    private Date created_date;
    private boolean status;

    public Todo() {
    }

    public Todo(int id, String task, Date created_date, boolean status) {
        this.id = id;
        this.task = task;
        this.created_date = created_date;
        this.status = status;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {

        this.id = id;
    }

    public String getTask() {

        return task;
    }

    public void setTask(String task) {
        this.task = task;
    }

    public Date getCreated_date() { return created_date; }

    public void setCreated_date(Date created_date) { this.created_date = created_date; }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }
}
