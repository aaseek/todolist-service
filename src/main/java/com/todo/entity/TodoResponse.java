package com.todo.entity;

import org.apache.logging.log4j.message.StringFormattedMessage;

import java.rmi.MarshalledObject;
import java.util.HashMap;
import java.util.Map;

public class TodoResponse {

    private String task;
    private boolean status;

    public TodoResponse() {
    }

    public TodoResponse(String task, boolean status) {
        this.task = task;
        this.status = status;
    }

    public String getTask() {
        return task;
    }

    public void setTask(String task) {
        this.task = task;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }


    public Map<String,Object> asMap(){
        Map<String,Object> paramMap = new HashMap<>();
        paramMap.put("task", this.task);
        paramMap.put("status",this.status);
        return paramMap;
    }
}
