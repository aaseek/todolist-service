package com.todo.service;

import com.todo.entity.Todo;
import com.todo.entity.TodoResponse;

import java.util.List;

public interface TodoService {

    TodoResponse addTask(TodoResponse todoResponse);
    List<Todo> getAllTask();
    int deleteTask(int id);
    int updateTask(Todo todo, int id);
    boolean checkDuplicateTask(String task);


}
