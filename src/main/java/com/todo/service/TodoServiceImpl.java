package com.todo.service;

import com.todo.entity.Todo;
import com.todo.entity.TodoResponse;
import com.todo.repository.TodoRepository;
import com.todo.service.TodoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class TodoServiceImpl implements TodoService {

    private TodoRepository todoRepository;

    @Autowired
    public TodoServiceImpl(TodoRepository todoRepository) {
        this.todoRepository = todoRepository;
    }

    @Override
    public TodoResponse addTask(TodoResponse todoResponse) {
        return todoRepository.addTask(todoResponse) ;
    }

    @Override
    public List<Todo> getAllTask() {
        return todoRepository.listAllTasks();
    }

    @Override
    public int deleteTask(int id) {
        return todoRepository.deleteTask(id);
    }

    @Override
    public int updateTask(Todo todo, int id) {
        return todoRepository.updateTodo(todo, id);
    }

    @Override
    public boolean checkDuplicateTask(String task) {

         List<Todo> listAllTasks = todoRepository.listAllTasks();
         Todo todoCheck = listAllTasks.stream()
                 .filter(todo -> task == todo.getTask())
                 .findAny()
                 .orElse(null);
         return todoCheck == null ? false : true;
    }
}

