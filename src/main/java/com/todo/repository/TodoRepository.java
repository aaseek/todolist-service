package com.todo.repository;

import com.todo.entity.Todo;
import com.todo.entity.TodoResponse;

import java.util.List;


public interface TodoRepository {

    TodoResponse addTask(TodoResponse todoResponse);
    List<Todo> listAllTasks();
    int deleteTask(int id);
    int updateTodo(Todo todo, int id);
}
