package com.todo.repository;

import com.todo.entity.Todo;
import com.todo.entity.TodoResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class TodoRepositoryImpl implements TodoRepository {

    private final NamedParameterJdbcTemplate jdbcTemplate;
    private static final Logger LOGGER = LoggerFactory.getLogger(TodoRepositoryImpl.class);

    @Autowired
    public TodoRepositoryImpl(NamedParameterJdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public TodoResponse addTask(TodoResponse todoResponse) {

        int result = jdbcTemplate.update(Queries.INSERT_TASK, todoResponse.asMap());
        LOGGER.debug("result after inserting -> {}", result);
        return todoResponse;

    }

    @Override
    public List<Todo> listAllTasks() {

        List<Todo> listAllTask = jdbcTemplate.query(Queries.GET_ALL_TASK, new RowMapper<Todo>() {
            @Override
            public Todo mapRow(ResultSet rs, int rowNum) throws SQLException {
                Todo todo = new Todo();
                todo.setId(rs.getInt("id"));
                todo.setTask(rs.getString("task"));
                todo.setCreated_date(rs.getDate("created_date"));
                todo.setStatus(rs.getBoolean("status"));
                return todo;
            }
        });
        return listAllTask;

    }

    @Override
    public int deleteTask(int id) {
        Map<String, Object> deleteTask = new HashMap<>();
        deleteTask.put("id", id);
        int result = jdbcTemplate.update(Queries.DELETE_TASK, deleteTask);
        return result;
    }


    @Override
    public int updateTodo(Todo todo, int id) {

        Map<String, Object> updateTask = new HashMap<>();
        updateTask.put("id", id);
        updateTask.put("task", todo.getTask());
        updateTask.put("status", todo.isStatus());
        int result = jdbcTemplate.update(Queries.UPDATE_TASK, updateTask);
        return result;

    }


    static class Queries {
        private static final String INSERT_TASK = "insert into todolist.todo (task,status) VALUES(:task,:status)";
        private static final String GET_ALL_TASK = "select * from todolist.todo";
        private static final String DELETE_TASK = "DELETE FROM todolist.todo where id=:id";
        private static final String UPDATE_TASK = "UPDATE todolist.todo set task=:task , status=:status where id=:id";
    }
}
