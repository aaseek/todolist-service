//package com.todo.repository;
//
//import com.todo.entity.Todo;
//import com.todo.entity.TodoResponse;
//import org.springframework.stereotype.Repository;
//
//import java.util.ArrayList;
//import java.util.List;
//
//@Repository("inmemory repository")
//public class TodoRepositoryImplinMemory implements TodoRepository {
//
//    private static final List<TodoResponse> taskList = new ArrayList<>();
//
//    public TodoRepositoryImplinMemory(){
//        prepareStaticData();
//    }
//
//
//    public List<TodoResponse> prepareStaticData(){
//        taskList.add(new TodoResponse("workout",true));
//        taskList.add(new TodoResponse("code quality check",false));
//        taskList.add(new TodoResponse("code coverage test", true));
//        return prepareStaticData();
//    }
//
//    @Override
//    public TodoResponse addTask(TodoResponse todoResponse) {
//        int indexTonewAdd = taskList.size();
//        if ( todoResponse == null){
//            throw new NullPointerException("data to save cannot be null");
//        }
//            taskList.add(indexTonewAdd, todoResponse);
//        return taskList.get(indexTonewAdd);
//    }
//
//
//    @Override
//    public List<Todo> listAllTasks() {
//        return null;
//    }
//
//    @Override
//    public int deleteTask(int id) {
//        return 0;
//    }
//
//    @Override
//    public int updateTodo(Todo todo, int id) {
//        return 0;
//    }
//}
