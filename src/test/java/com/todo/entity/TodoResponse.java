package com.todo.entity;

public class TodoResponse {

    private String task;
    private boolean status;

    public TodoResponse() {
    }

    public TodoResponse(String task, boolean status) {
        this.task = task;
        this.status = status;
    }

    public String getTask() {
        return task;
    }

    public void setTask(String task) {
        this.task = task;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }
}
