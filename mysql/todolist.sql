CREATE DATABASE todolist;

CREATE TABLE IF NOT EXISTS todolist.todo(
                        id INT primary key auto_increment,
                        task VARCHAR(255),
                        created_date DATETIME not null default NOW(),
                        status BOOLEAN not null default false
                        );


Insert into todolist.todo (task,status) VALUES(:task,:status);